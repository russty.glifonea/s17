/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	function sampleprompt(){
		let fullName = prompt('Enter your fullname');
		let age = prompt('Enter your age');
		let location = prompt('Enter your location');

		console.log("Hello, " + fullName);
		console.log("You are " + age + " " + "years old");
		console.log("You live in " + location);
	};

	sampleprompt();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function displayTopMusicArtist(){
		console.log("1. Lady Gaga");
		console.log("2. Katy Perry");
		console.log("3. Harry Styles");
		console.log("4. One Direction");
		console.log("5. Miley Cyrus");
	}

	displayTopMusicArtist();


/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	function displayTopFavMovies(){
		console.log("1. The Shindler's List");
		console.log("Rotten Tomatoes Ratings: 98%");
		console.log("2. Knives Out");
		console.log("Rotten Tomatoes Ratings: 97%");
		console.log("3. Spencer");
		console.log("Rotten Tomatoes Ratings: 81%")
		console.log("4. Dont Look Up");
		console.log("Rotten Tomatoes Ratings: 76%")
		console.log("5. Joker");
		console.log("Rotten Tomatoes Ratings: 68%")
	}

	displayTopFavMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


function printFriends(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};


printFriends();
